#!/bin/bash

printf "\n\n===> Run './finish-install.sh' after your machine restarts.\n\n"
sleep 4


printf "\n=> Adding Ansible repository...\n"
echo 'deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main' | sudo tee -a /etc/apt/sources.list


printf "\n=> Adding some packages...\n"
sudo apt-get -y install git curl </dev/null


printf "\n=> Installing Ansible...\n"
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
sudo apt update -y && apt install --fix-missing
sudo apt-get -y install ansible </dev/null


printf "\n=> Adding hosts for Ansible...\n"
sudo mkdir -p /etc/ansible
echo 'localhost' | sudo tee -a /etc/ansible/hosts > /dev/null


printf "\n=> Running Ansible playbook now!\n"
mkdir src && cd src
git clone https://codeberg.org/etienne/ansible-debian.git

export ANSIBLE_CFG="${ROOT}/ansible.cfg"
