# Ansible Debian

Ansible playbook that installs software for Debian and config for XFCE.

### Before running the install script
```
su - root
apt install sudo wget
adduser <user> sudo
```
Exit, log out, and log back in as `<user>`

Then, as `<user>`:
```
wget -qO- install.etiennebaque.com | bash
```
