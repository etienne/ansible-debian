#!/usr/bin/zsh

clear
echo "==============================================="
echo "=       Debian - finishing installation       ="
echo "==============================================="
sleep 1

printf "\n===> Installing all dotfiles and vim plugins...\n"
env RCRC=$HOME/dotfiles/rcrc rcup

printf "===> Reloading shell now. All done! :)\n\n"

exec $SHELL
