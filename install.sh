#!/bin/bash

echo "========================================="
echo "=   Debian - installation of packages   ="
echo "========================================="

wget -O ~/base-install.sh https://codeberg.org/etienne/ansible-debian/raw/branch/develop/base-install.sh

cd ~ && sh ./base-install.sh

rm ~/base-install.sh
cd ~/src/ansible-debian

ansible-playbook --ask-become-pass "$@" site.yml
